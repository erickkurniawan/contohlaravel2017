<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TodoList;
use App\BLL\TodoListBLL;
use Validator;
use Illuminate\Support\Facades\Auth;

class TodoListController extends Controller
{
    /*public function __construct(){
        if (!Auth::check()) {
            return "Unauthorize";
        }    
    }*/
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = false;
        if (Auth::check()) {
            $status = $request->user()->authorizeRoles(['manager']);
        }
        else {
            return redirect()->route('login');
        }
        
        $todo_lists = TodoListBLL::GetAllOrderAsc();
        
        //$todo_lists = TodoList::orderBy('name','asc')->get();

        return view('todos.index',array('todo_lists'=>$todo_lists,
            'status'=>$status));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('todos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = ['required'=>':attribute name harus diisi ya !'];

        //$this->validate($request,['name'=>'required|unique']);
        $validator = Validator::make($request->all(),[
            'title'=>'required'
        ],$messages);

        if($validator->fails()){
            return redirect('todos/create')->withErrors($validator)->withInput();
        }

        $name = $request->get('name');
        
        $newtodo = new TodoList();
        $newtodo->name=$name;
        $newtodo->save();

        return redirect()->route('todos.index');
    }

    public function messages()
    {
        return [
            'title.required' => 'Title harus diisi !'
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $list = TodoList::find($id);
        return view('todos.show')->withList($list);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list = TodoList::find($id);
        return view('todos.edit')->with('list',$list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->get('name');

        $list = TodoList::findOrFail($id);
        
        //return $name." ".$list->name;
        $list->name = $name;
        $list->update();
        
        
        return redirect()->route('todos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
