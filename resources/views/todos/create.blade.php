@extends('layouts.main')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">Home</a>
    </li>
    <li class="breadcrumb-item active">Create</li>
@endsection

@section('content')
<div class="col-md-12">
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('todos.store') }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name :</label>
            <input type="text" name="name" class="form-control" />
        </div>
        <button type="submit" class="btn btn-info btn-sm">Add</button>
    </form>
</div>

@endsection