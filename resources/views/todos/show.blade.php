@extends('layouts.main')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">Home</a>
    </li>
    <li class="breadcrumb-item active">Tampil</li>
@endsection

@section('content')
<div class="col-md-12">
    <h2>Show Data</h2>
    <p>ID : {{ $list->id }} dan Name : {{ $list->name }}</p>
</div>

@endsection