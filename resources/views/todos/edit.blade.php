@extends('layouts.main')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">Home</a>
    </li>
    <li class="breadcrumb-item active">Create</li>
@endsection

@section('content')

<div class="col-md-12">
    <form method="POST" action="{{ route('todos.update',['id'=>$list->id]) }}">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name :</label>
            <input type="text" name="name" value="{{ $list->name }}" class="form-control" />
        </div>
        <button type="submit" class="btn btn-info btn-sm">Update</button>
    </form>
</div>

@endsection