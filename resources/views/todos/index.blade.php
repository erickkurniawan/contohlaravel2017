@extends('layouts.main')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">Home</a>
    </li>
    <li class="breadcrumb-item active">Todo Lists</li>
@endsection

@section('content')
<div class="col-md-12">
    <a href="{{ route('todos.create') }}" class="btn btn-info btn-sm">Add Todo</a><br>
    <h2>Daftar Todo</h2>
    <br/>
    <table class="table table-striped">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th></th>
        </tr>
        @foreach($todo_lists as $list)
            <tr>
                <td>{{ $list->id }}</td>
                <td>{{ $list->name }}</td>
               
                <td>
                @if($status==true)
                <a href="{{ route('todos.show',['id'=>$list->id]) }}" class="btn btn-success btn-sm">detail</a>
                &nbsp;
                <a href="{{ route('todos.edit',['id'=>$list->id]) }}" class="btn btn-warning btn-sm">edit</a>
                @endif
                </td>
            </tr>
        @endforeach
    </table>
</div>

@endsection